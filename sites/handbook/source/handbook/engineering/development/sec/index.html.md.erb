---
layout: handbook-page-toc
title: Sec Section
description: >-
  The Sec Section is composed of development teams working on Secure
  and Protect features of the GitLab DevOps Platform.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<%= devops_diagram(["Secure","Protect"]) %>

## Teams and Handbook Pages

The following teams comprise the sub-department:

- Protect stage - [handbook](/handbook/engineering/development/protect/)
  - Container Security group
- Secure stage - [handbook](/handbook/engineering/development/secure/)
  - Composition Analysis group - [handbook](/handbook/engineering/development/secure/composition-analysis/)
  - Dynamic Analysis group - [handbook](/handbook/engineering/development/secure/dynamic-analysis/)
  - Vulnerability Research group - [handbook](/handbook/engineering/development/secure/vulnerability-research/)
  - Threat Insights group - [handbook](/handbook/engineering/development/secure/threat-insights/)
- Anti-Abuse stage - [handbook](/handbook/engineering/development/anti-abuse/)

## Product Direction

Product direction can be found on the [Sec Section Product Direction](/direction/security/) handbook page.

## Performance Indicators

- [Sec Sub-department Performance Indicators](/handbook/engineering/metrics/sec/)
- [Error Budgets](/handbook/engineering/error-budgets/) as Performance Indicators for stage groups

## Slack channels

- [#sec-section](https://gitlab.slack.com/archives/C02087FTL5V) - Sec Section discussions spanning the Protect, and Secure stages.
- [#sec-growth-modelops-people-leaders](https://gitlab.slack.com/archives/C033F69CQCB) - Engineering people leaders in Sec, Growth, and ModelOps.
- [🔒sec-growth-modelops-leadership-confidential](https://gitlab.slack.com/archives/GKWF00Y3E) - Private channel for engineering people leaders in Sec, Growth, and ModelOps.

## Planning in the Section

In the vast majority of cases, work is scoped to individual groups within the section. However, there are times when the section needs to design
and execute solutions as a coordinated Section or risk creating poor and non-cohesive user experiences.

These initiatives will be orchestrated through epics and issues. Initiatives with the following labels are deemed to fall in this category of work.

- [`~section::sec` and `~group::not owned`](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=section%3A%3Asec&label_name%5B%5D=group%3A%3Anot_owned); or
- [`all Sec groups`](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_asc&state=opened&label_name%5B%5D=all%20Sec%20groups)

### Process for planning section-wide initiatives

At least once per milestone, Senior Engineering Managers in the section will do the following:

- In partnership with Product Management, initiatives 6 months or older will be evaluated to determine if they're still relevant.
- New initiatives will be triaged, checking their requirements for understandability and completeness. Further, the group most impacted will be identified.
  - In situations where most impacted group is not clear, the Architectural Council will be engaged to help discern which group that might be.
- Group most impacted will be declared DRI for that initiative and are expected to:
  - Produce a high-level implementation plan that will scale for the whole problem.
  - Create implementation issues that are broken down by feature category.
    - The original high-level implementation plan will be included, or at least directly linked, in the created issues.
    - Original issue where implementation plan was debated and created will also be linked to the generated issues.
  - Distribute implementation issues to the relevant groups.

Generated issues will be worked through normal prioritization processes as they are distributed to individual groups.

## Councils and Chapters

### Architectural Council (Slack [#s_secure-architectural-council](https://gitlab.slack.com/archives/C012RKVK231))

[Snippet to use in issues](https://gitlab.com/gitlab-com/www-gitlab-com/-/snippets/2126492).

In order to help the Sec section come to resolution on defining approaches to section-wide issues,
we have formed an Architectural Council. This group comprises tech leads (and SMEs to provide context)
in order to keep the team small and nimble. The group is a non-authoritative, supportive group whose members provide representative insights from their respective teams.

* Tech leads will serve as the representative for their team
* An issue should be created to capture the discussion that covers:
  * What is the issue at hand and what is the preferred action
  * What are the potential solutions and their associated pros/cons
  * What approach was decided and why
  * 3-business day [SLO](https://docs.gitlab.com/ee/development/code_review.html#review-turnaround-time)
    * We need to know when to start the clock on these as some issues can exist for months before being brought to the council. In this case, we should define within the issue when the clock does start.
* There will be a DRI assigned to the issue that is being discussed
* The DRI will be decided by either who has to address the issue first or where the code change is occurring the most (in that order)
* The issue will be discussed and the DRI will be the ultimate decision-maker for the approach taken
* Issues will be tackled on a First-In-and-First-Out (FIFO) order. Attempts to minimize SLA overlap should be made to prevent scheduling conflicts between member’s time

#### Considerations when determining the approach

* Simplicity and elegance
* Portability and/or modularity
* Supportability
* Maintainability
* Scalability
* Extensibility
* Reliability
* Security
* Cost (will it impact .com for large customers)
* Performance (speed and accuracy)
* Consistency (Fit with existing code base)

#### Capturing Resolved/Discovered Standards

Common scenarios/architectural concepts will likely be resolved/discovered. These should be captured generically in an architectural guidelines page of the handbook and, if possible, codified into our processes/templates.

##### Scope

The table below captures characteristics (requirements?) of work that is in-scope, opt-in, or out-of-scope. All requirements must be met for each category.

| Reason/Condition | In-Scope | Opt-in | Out-of-Scope |
|------------------|----------|--------|--------------|
| Does not involve architectural decisions | | | x |
| Is after-the-fact | | | x |
| Is not already covered by architecture guidelines/handbook | x | x | |
| Has broad impact within #secure[^1] | x | | |
| Is a new unit of work | x | x | |
| Is strictly #secure or #protect | x | x |  |
| Could not come to an agreement (escalation) | | ? | |
| Involves architecture decisions | x | x | |

[^1]: Meaning that the proposed work requires knowledge or impacts multiple groups

#### Acceptance Criteria

GitLab’s Stance for Architectural issues: https://about.gitlab.com/handbook/engineering/architecture/

#### Team Representatives

| Team | Representative |
| --- | --- |
| [Composition Analysis](#composition-analysis) | <%= link_to_team_member('fcatteau') %>  |
| [Container Scanning](/handbook/engineering/development/protect/) | <%= link_to_team_member('mparuszewski') %> |
| [Dynamic Analysis](#dynamic-analysis) | <%= link_to_team_member('cam_swords') %> |
| [Static Analysis](#static-analysis) | <%= link_to_team_member('theoretick') %>  |
| [Threat Insights](#threat-insights) | <%= link_to_team_member('minac') %> |
| [Frontend](#threat-insights) | <%= link_to_team_member('svedova') %> |
| [Vulnerability Research](#vulnerability-research) | <%= link_to_team_member('idawson') %>, <%= link_to_team_member('julianthome') %> |

### Frontend Chapter (Slack [#secure-frontend-chapter](https://gitlab.slack.com/archives/C031RSQ72S2))

The Sec Section Frontend Chapter at GitLab is a federation of all Frontend Engineers and team members interested in frontend-related topics within the Sec Section.

The main purpose of the Frontend chapter is to connect Frontend Engineers within the Secure Group,
share knowledge, exchange about common challenges and leverage their collaboration.

The chapter was founded when Secure transitioned to Fullstack Teams which resulted in some Engineers being
the isolated experts within their team.

The members of the chapter exchange through the [slack-channel](https://gitlab.slack.com/archives/C031RSQ72S2), there is also a sync meeting twice a month on the Secure Group Calendar.

There is no strict rulebook of how chapter-members collaborate with each other. The chapter is about
visibility of challenges that might be similar amongst the different teams within the secure-group and how we can
benefit from this.

#### Team Representatives

| Team                                          | Representative                                    |
| --------------------------------------------- | ------------------------------------------------- |
| [Composition Analysis](#composition-analysis) | <%= link_to_team_member('farias-gl') %>           |
| [Static Analysis](#static-analysis)           | <%= link_to_team_member('jannik_lehmann') %>      |
| [Dynamic Analysis](#dynamic-analysis)         | <%= link_to_team_member('arfedoro') %>            |
| [Dynamic Analysis](#dynamic-analysis)         | <%= link_to_team_member('djadmin') %>             |
| [Threat Insights](#threat-insights)           | <%= link_to_team_member('dftian') %>              |
| [Threat Insights](#threat-insights)           | <%= link_to_team_member('dpisek') %>              |
| [Threat Insights](#threat-insights)           | <%= link_to_team_member('pgascouvaillancourt') %> |
| [Threat Insights](#threat-insights)           | <%= link_to_team_member('sming-gitlab') %>        |
| [Threat Insights](#threat-insights)           | <%= link_to_team_member('svedova') %>             |

#### Page Performance

Our team monitors [LCP](https://about.gitlab.com/handbook/engineering/development/performance-indicators/#largest-contentful-paint-lcp) (Largest Contentful Paint) to ensure performance is below our target (currently 2500ms).

[LCP Dashboard for Secure owned pages](https://dashboards.gitlab.net/d/sftijGFMz/sitespeed-lcp-leaderboard?from=now-90d&orgId=1&to=now&refresh=30s&var-namespace=sitespeed_io&var-path=desktop&var-testname=gitlab&var-domains=gitlab_com&var-pages=API_Fuzzing_Config_UI&var-pages=DAST_Profiles&var-pages=On_Demand_Scans&var-pages=SAST_Config_UI&var-pages=Secure_Dependency_List&var-pages=Secure_License_Compliance&var-pages=Secure_Security_Configuration&var-pages=DAST_Config_UI&var-browser=chrome&var-function=median&var-connectivity=cable)

#### How to work with the Quality team

#### Frontend Responsibilities

1. Being able to identify what code changes would likely break E2E or System level tests and informing Quality.
1. Not to write E2E tests, but to catch potential failures and communicate gaps in coverage before landing to master.

#### Identifying potential breakages

1. Look to see if issue you are working on [has existing test coverage](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/736). These are the tests likely to fail

1. If you are working around code that contains a selector like `data-qa-selector="<name>"`, then there is likely to be an existing E2E test. Tests can be found by searching our [E2E tests in Secure](https://gitlab.com/gitlab-org/gitlab/-/tree/master/qa/qa/specs/features/ee/browser_ui/secure).

#### Communicating changes that may break tests

Ping the DRI for quality assigned to Secure. You can find the person on the [team page](https://about.gitlab.com/handbook/engineering/development/secure/#team-members). If they are unavailable, then #quality on slack or the [triage DRI](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/#schedule) dependent on severity.

### Section Retrospectives

In addition to our group retrospectives, we hold a Sec Section level retrospective each month. The goal of the section wide retrospective is to review topics that bubbled-up from our group/team retrospectives. Additionally, we identify themes that could be discussed synchronously.  We use [this doc](https://docs.google.com/document/d/1g_FIMgr9r_Yf56xISxoI8B-1G-kbP3PQSeo7W-kKj24/edit#) to facilitate the section retrospective.

#### Key Dates

1. 26th of each month - Group async retrospective issues are generated. Groups should start contributing topics.
1. Week of the 17th - Groups hold their retrospectives. Team members bubble-up identified topics and follow-up items (outcomes) to the [section retrospective document](https://docs.google.com/document/d/1g_FIMgr9r_Yf56xISxoI8B-1G-kbP3PQSeo7W-kKj24/edit#).
1. Week of the 22nd -  Section wide retrospective sync. AMER/EMEA at 7:30am PT / 2:30pm UTC, APAC at 3:30pm PT / 10:30pm UTC.


#### DRI Responsibilities

The [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) for Section-wide retrospectives will be the Senior Engineering Manager. The SEM will find a volunteer if it is needed on specific milestones. The following tasks are executed each milestone:


1. Prior to the section sync, review bubble-up topics and identify up with 2-3 themes to support sync discussion topics.
1. Ask everyone through Slack to vote on which topics they would like to spend time discussing as a section.
1. Follow up with groups on any identified improvements.
1. Promote, promote, promote!
