---
layout: secure_and_protect_direction
title: "Category Direction - API Security"
description: "API Security is focused on securing APIs through DAST API scanning and API fuzz testing."
canonical_path: "/direction/secure/dynamic-analysis/api-security/"
---

- TOC
{:toc}
## Secure

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2021-11-10` |

## Description
### Introduction and how you can help

Thank you for visiting this category direction page on API Security Testing at GitLab. This page belongs to the Dynamic Analysis group of the Secure stage and is maintained by Derek Ferguson ([dferguson@gitlab.com](mailto:<dferguson@gitlab.com>)).

This direction page is a work in progress and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAPI%20Security) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for API Security, we'd especially love to hear from you.

### Overview

API Security is a broad category that incorporates everything from API discovery and management to DAST API and API fuzz testing to active API defense. GitLab's initial focus in API Security will be on DAST API scanning and API fuzz testing. As the category matures, we will explore expanding into API discovery and management and active API defense.

> GitLab was recently named as a [Challenger in the 2021 Magic Quadrant for Application Security Testing](https://about.gitlab.com/analysts/gartner-ast21/).

API Security currently supports REST, GraphQL, and SOAP APIs.

As API security has historically been the domain of security teams, rather than developers, testing the API in its running state often is overlooked during the development process. Since this type of testing requires an application to be deployed, the attack surface of the running application is usually not evaluated until the application has passed through the development cycle and is deployed to a staging server (or worse, to production!). Testing this late in the SDLC means that developers have little to no time to respond to any vulnerabilities that are found and trade-offs must be made between fixing vulnerabilities and releasing on time. This can lead to vulnerabilities being released into production either as a calculated risk or with no knowledge of the vulnerability at all. 

We see API security testing as an ideal collaboration point between established security teams and developers, leading to finding and fixing vulnerabilities earlier in the SDLC and reducing the number of vulnerabilities released to production. By integrating API security into their pipelines and leveraging review apps to dynamically test applications, developers can be more conscious of the security impact of their code on the running application. This awareness enables them to take the initiative to fix these issues before merging features into the default branch. Similarly, the security team member can take a proactive, rather than reactive, approach to security by reviewing pipeline results or running on-demand scans and creating issues for any vulnerability found earlier in the SDLC. All of this allows these teams to work together to reduce the overall risk of deploying new code to a production application.

### Goal

Our goal is to provide API Security scanning as part of the standard development process. This means that API security is executed every time a new review app is available or a build is deployed to a server. 

Since API Security requires a running application, we can provide results for feature branches leveraging [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/), temporary environments that run the modified version of the application. We can also provide results for applications running on other servers, such as staging or development environments, either through a CI/CD pipeline scan or a manually-triggered on-demand scan.

API Security results can be consumed in the merge request, where new vulnerabilities are shown. A full report is available in the pipeline details page.

API Security results are also a part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status of their projects.

We also want to ensure that the production environment is always secure by allowing users to run an on-demand API Security scan on a deployed app even if there is no change in the code. On-demand scans will allow for out-of-band security testing and issue reproduction, without needing any code changes or merge requests to start a scan.

### Roadmap

- [API Security scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254) (Beta released in 14.1)
- [API Security scanner speed improvements](https://gitlab.com/groups/gitlab-org/-/epics/6502)
- [API Security gRPC support](https://gitlab.com/gitlab-org/gitlab/-/issues/244492)
- [API Security Parameter exclusion](https://gitlab.com/gitlab-org/gitlab/-/issues/292196)
- [API Security support for dotenv based dynamic environments](https://gitlab.com/gitlab-org/gitlab/-/issues/247641)
- [API Security x-request-id header](https://gitlab.com/gitlab-org/gitlab/-/issues/329722)
- [API Security sample data generation for XML](https://gitlab.com/gitlab-org/gitlab/-/issues/320842)
- [API Security Authentication improvements](https://gitlab.com/groups/gitlab-org/-/epics/3932)

## What's Next & Why

We are integrating the Peach API Security scanner into DAST to give us an immediate and major improvement in DAST API scanning coverage, configuration, and confidence. As soon as the API Security scanner is integrated, we will gain capabilities that will increase the number and type of APIs that we are able to scan. The new scanner allows for specifying API endpoints via Postman collections and HAR files, adding onto the OpenAPI specification we support currently. It also gives access to scan GraphQL and SOAP APIs, rather than being limited to REST APIs. Improved authentication support for more authentication methods is another major improvement that we will gain with the integration of the new scnner. This scanner was released as a beta feature in GitLab 14.1 and we are actively working on issues to release it as a GA feature.
- [API Security scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)

As a part of bringing the API Security scanner to GA for DAST API, we are working on a number of speed improvements. Integrating any security test into the CI/CD pipeline brings the question of how long these tests will take to complete. We believe that while our API Security scanning is already fast enough for a large number of users, there is still room for some major improvements. We have already seen improvements of ~30% faster tests and expected to see even faster test times as more optimizations are put in place.
- [API Security scanner speed improvements](https://gitlab.com/groups/gitlab-org/-/epics/6502)

## Competitive Landscape

- [Synopsys](https://www.synopsys.com/software-integrity/security-testing/api-security-testing.html)
- [Netsparker](https://www.netsparker.com/)
- [StackHawk](https://www.stackhawk.com/)
- [Salt Security](https://salt.security/)
- [Imperva](https://www.imperva.com/products/api-security/)
- [Mulesoft](https://www.mulesoft.com/platform/enterprise-integration)

We have an advantage of being able to provide testing results before the app is deployed into the production environment, by using Review Apps. This means that we can provide API security scan results for every single commit. The easy integration of API security scanning early in the software development life cycle is a unique position that GitLab has in the API Security market. Integrating other tools at this stage of the SDLC is typically difficult, at best.

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

* [2021 Gartner Magic Quadrant: Application Security Testing](https://about.gitlab.com/analysts/gartner-ast21/)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [2019 Forester State of Application Security](https://www.forrester.com/report/The+State+Of+Application+Security+2019/-/E-RES145135)

## Top Vision Item(s)

- [API Security scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)
